extends Control

# The about scene is pretty self-explanatory, it show the about information
# related to the current app

func _ready():
	find_node("TextParentsInstructions").set_text(loadParentsInstructionsContent())
	find_node("TextParentsInstructions").set_selection_enabled(true)

# This load the content of the parents instructions section, to know which content
# should be loaded it takes the arguments given when the scene changed
# which means when the about button pressed (see parents_instructions.tscn/.gd)
func loadParentsInstructionsContent() -> String:
	var currentApp: String = Global.get_arguments()[0]
	if(currentApp != null):
		var file = File.new()
		file.open("res://data/artiphonie/parentsInstructions.txt", File.READ)
		var content = file.get_as_text()
		file.close()
		return content
	else:
		return ""
