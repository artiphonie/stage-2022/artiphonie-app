Bienvenue dans ARTIPHONIE,
Cette application va permettre à votre enfant de travailler, sous votre supervision, ses habiletés pour articuler les sons de notre langue et c’est votre orthophoniste qui fera la sélection des mots. 
Pour aider votre enfant dans cet apprentissage, le programme contient des soutiens en images : les mots à travailler vont être oralisés par la tablette, écrits en partition de gestes, et également en images. 
Dans un première partie nommée APPRENDRE, vous pouvez revoir tous les sons de la langue, classés par famille, avec le geste correspondant en vidéo.
Ensuite, dans la partie JE M'ENTRAÎNE, une image apparaît avec la partition en gestes et l’image et votre enfant va devoir produire le mot et vous devrez valider sa réponse.
Ensuite, dans la partie JE JOUE, 3 types d’activités plus ludiques sont proposées pour continuer à entraîner la production de ses mots : un jeu de l’oie, un jeu d’écoute et un mémory.
Tout le travail engagé par votre enfant génère des récompenses pour l’aider à rester motivé. Les résultats de son travail sont adressés à son orthophoniste qui peut suivre les progrès. 

