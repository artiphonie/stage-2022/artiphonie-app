1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="fr.LinguAnimals"
4    android:installLocation="auto"
5    android:versionCode="3"
6    android:versionName="3" > <!-- glEsVersion is modified by the exporter, changing this value here has no effect. -->
7    <uses-sdk
8        android:minSdkVersion="19"
9        android:targetSdkVersion="30" />
10
11    <supports-screens
11-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:12:5-16:40
12        android:largeScreens="true"
12-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:15:9-36
13        android:normalScreens="true"
13-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:14:9-37
14        android:smallScreens="true"
14-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:13:9-36
15        android:xlargeScreens="true" />
15-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:16:9-37
16
17    <uses-feature
17-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:10:5-78
18        android:glEsVersion="0x00030000"
18-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:10:19-51
19        android:required="true" />
19-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:10:52-75
20
21    <uses-permission android:name="android.permission.INTERNET" />
21-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:11:5-67
21-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:11:22-64
22    <uses-permission
22-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:12:5-107
23        android:name="android.permission.READ_EXTERNAL_STORAGE"
23-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:12:22-77
24        android:maxSdkVersion="29" />
24-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:12:78-104
25    <uses-permission
25-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:13:5-108
26        android:name="android.permission.WRITE_EXTERNAL_STORAGE"
26-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:13:22-78
27        android:maxSdkVersion="29" />
27-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:13:79-105
28
29    <instrumentation
29-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:14:5-19:51
30        android:name=".GodotInstrumentation"
30-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:16:9-45
31        android:icon="@mipmap/icon"
31-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:17:9-36
32        android:label="@string/godot_project_name_string"
32-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:18:9-58
33        android:targetPackage="fr.LinguAnimals" />
33-->/home/kura/Stage/Artiphonie-app/android/build/src/release/AndroidManifest.xml:19:9-48
34
35    <application
35-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:31:5-87:19
36        android:allowBackup="false"
36-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:31:68-95
37        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
37-->[androidx.core:core:1.2.0] /home/kura/.gradle/caches/transforms-3/f468396e8f36529b92fb98842a149f3b/transformed/core-1.2.0/AndroidManifest.xml:24:18-86
38        android:hasFragileUserData="false"
38-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:31:118-152
39        android:icon="@mipmap/icon"
39-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:31:238-265
40        android:isGame="true"
40-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:31:96-117
41        android:label="@string/godot_project_name_string"
41-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:31:18-67
42        android:requestLegacyExternalStorage="true" >
42-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:31:153-197
43        <activity
43-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:59:9-81:20
44            android:name="com.godot.game.GodotApp"
44-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:60:13-37
45            android:configChanges="orientation|keyboardHidden|screenSize|smallestScreenSize|density|keyboard|navigation|screenLayout|uiMode"
45-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:66:13-141
46            android:excludeFromRecents="false"
46-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:64:13-47
47            android:label="@string/godot_project_name_string"
47-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:61:13-62
48            android:launchMode="singleTask"
48-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:63:13-44
49            android:resizeableActivity="false"
49-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:67:13-47
50            android:screenOrientation="landscape"
50-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:65:13-50
51            android:theme="@style/GodotAppSplashTheme" >
51-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:62:13-55
52            <intent-filter>
52-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:73:13-80:29
53                <action android:name="android.intent.action.MAIN" />
53-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:74:17-69
53-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:74:25-66
54
55                <category android:name="android.intent.category.LAUNCHER" />
55-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:75:17-77
55-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:75:27-74
56
57                <!--
58                Enable access to OpenXR on Oculus mobile devices, no-op on other Android
59                platforms.
60                -->
61                <category android:name="com.oculus.intent.category.VR" />
61-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:79:17-74
61-->/home/kura/Stage/Artiphonie-app/android/build/AndroidManifest.xml:79:27-71
62            </intent-filter>
63        </activity>
64        <!-- Records the version of the Godot editor used for building -->
65        <meta-data
66            android:name="org.godotengine.editor.version"
67            android:value="3.4.4.stable" />
68        <meta-data
68-->[GodotTTS.1.0.0.release.aar] /home/kura/.gradle/caches/transforms-3/22b5d45566d39cf44dfdeff26ef272b6/transformed/jetified-GodotTTS.1.0.0.release/AndroidManifest.xml:10:9-12:75
69            android:name="org.godotengine.plugin.v1.GodotTTS"
69-->[GodotTTS.1.0.0.release.aar] /home/kura/.gradle/caches/transforms-3/22b5d45566d39cf44dfdeff26ef272b6/transformed/jetified-GodotTTS.1.0.0.release/AndroidManifest.xml:11:13-62
70            android:value="ibardinov.godot.plugin.android.tts.GodotTTS" /> <!-- Records the version of the Godot library -->
70-->[GodotTTS.1.0.0.release.aar] /home/kura/.gradle/caches/transforms-3/22b5d45566d39cf44dfdeff26ef272b6/transformed/jetified-GodotTTS.1.0.0.release/AndroidManifest.xml:12:13-72
71        <meta-data
71-->[godot-lib.release.aar] /home/kura/.gradle/caches/transforms-3/116885a8b5e7a96710277a2f2a84ccfd/transformed/jetified-godot-lib.release/AndroidManifest.xml:19:9-21:44
72            android:name="org.godotengine.library.version"
72-->[godot-lib.release.aar] /home/kura/.gradle/caches/transforms-3/116885a8b5e7a96710277a2f2a84ccfd/transformed/jetified-godot-lib.release/AndroidManifest.xml:20:13-59
73            android:value="3.4.4.stable" />
73-->[godot-lib.release.aar] /home/kura/.gradle/caches/transforms-3/116885a8b5e7a96710277a2f2a84ccfd/transformed/jetified-godot-lib.release/AndroidManifest.xml:21:13-41
74
75        <service android:name="org.godotengine.godot.GodotDownloaderService" />
75-->[godot-lib.release.aar] /home/kura/.gradle/caches/transforms-3/116885a8b5e7a96710277a2f2a84ccfd/transformed/jetified-godot-lib.release/AndroidManifest.xml:23:9-80
75-->[godot-lib.release.aar] /home/kura/.gradle/caches/transforms-3/116885a8b5e7a96710277a2f2a84ccfd/transformed/jetified-godot-lib.release/AndroidManifest.xml:23:18-77
76    </application>
77
78</manifest>
